const express = require('express');
const cors = require('cors');
const { config } = require('dotenv');
const app = express();
const connection = require('./database/db.connection');
config();

const PORT = process.env.PORT || 4500;
connection();
app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json({ limit: '500mb' }));
app.use('/api', require('./routers/index.router'));

// Inicia el servidor
app.listen(PORT, () => {
  console.log(`Servidor en ejecución en el puerto ${PORT}`);
});