const preRegistroSchema = require('../schema/preRegistro.schema');
const pacienteSchema = require('../schema/pacientes.schema');
const pruebasSchema = require('../schema/pruebas.schema');

const registerPreRegistro = async(req, res) => {
    try {
        const body = req.body;

        const nuevoPreRegistro = new preRegistroSchema(body);
      
          // Guardar el nuevo documento en la base de datos
          nuevoPreRegistro.save()
            .then((registroGuardado) => {
              return res.status(201).json({
                ok: true,
                msg: "Pre-Registro Guardado"
              })
            })
            .catch((error) => {
                return res.status(400).json({
                    ok: false,
                    msg: "Error al guardar el registro",
                    err: error
                })
              console.error('Error al guardar el registro:', error);
            })
    } catch(err) {
        return res.status(500).json({
            ok: false,
            msg: "Error al registrar los datos suministrados"
        });
    }
}

const registerPaciente = async(req, res) => {
    try {
        const body = req.body;
        const nuevoPaciente = new pacienteSchema({
            name: body.name,
            lastName: body.lastName,
            email: body.email,
            dependiente: body.dependiente
        });

        nuevoPaciente.save()
        .then((registroGuardado) => {
          return res.status(201).json({
            ok: true,
            msg: "Paciente Guardado"
          })
        })
        .catch((error) => {
            return res.status(400).json({
                ok: false,
                msg: "Error al guardar el registro",
                err: error
            })
          console.error('Error al guardar el registro:', error);
        })
    } catch(err) {
        return res.status(500).json({
            ok: false,
            msg: "Error al registrar paciente"
        })
    }
}

const getPacientes = async(req, res) => {
    try {
        const pacientes = pacienteSchema.find()
        .then((data) => {
            return res.status(200).json({
                ok: true,
                data: data
            });
        })
    } catch(err) {
        console.log(err);
        return res.status(500).json({
            ok: false,
            msg: "Error al ver paciente",
            error: err
        });
    }
}

const getPreRegistros = async(req, res) => {
    try {
        const preRegistro = preRegistroSchema.find()
        .then((data) => {
            return res.status(200).json({
                ok: true,
                data: data
            });
        })
    } catch(err) {
        console.log(err);
        return res.status(500).json({
            ok: false,
            msg: "Error al ver pre-registro",
            error: err
        }); 
    }
}

const registerPruebas = async(req, res) => {
    try {
        const body = req.body;
        const newPruebas = new pruebasSchema({
            nombre: body.name,
            costo: body.price
        });

        newPruebas.save()
        .then((data) => {
            return res.status(200).json({
                ok: true,
                msg: "Prueba registrada correctamente"
            });
        })
    } catch(err) {
        console.log(err);
        return res.status(500).json({
            ok: false,
            msg: "Error al registrar",
            error: err
        }); 
    }
}

const getPruebas = async(req, res) => {
    try {
        const pruebas = pruebasSchema.find()
        .then((data) => {
            return res.status(200).json({
                ok: true,
                data: data
            });
        })
    } catch(err) {
        console.log(err);
        return res.status(500).json({
            ok: false,
            msg: "Error al ver",
            error: err
        }); 
    }
}

const getPreRegistroById = async(req, res) => {
    try {
        const { id } = req.params;

        const preRegistro = await preRegistroSchema.findById(id)
        .then((data) => {
            return res.status(200).json({
                ok: true,
                data: data
            })
        })
    } catch(err) {
        console.log(err);
        return res.status(500).json({
            ok: false,
            msg: "Error al ver",
            error: err
        }); 
    }
}

const updateState = async(req, res) => {
    try {
        const { id } = req.params;

        const pre = await preRegistroSchema.findByIdAndUpdate(id, { estado: "PAGADO" });

        return res.status(200).json({
            ok: true,
            msg: "Pagado"
        })
    } catch(err) {
        console.log(err);
        return res.status(500).json({
            ok: false,
            msg: "Error al ver",
            error: err
        }); 
    }
}

module.exports = {
    registerPreRegistro,
    registerPaciente,
    getPacientes,
    getPreRegistros,
    registerPruebas,
    getPruebas,
    getPreRegistroById,
    updateState
}