const router = require('express').Router();
const controller = require('../controllers/preRegistro.controller');

router.post('/registrar', controller.registerPreRegistro);
router.post('/registrar/paciente', controller.registerPaciente);
router.get('/pacientes', controller.getPacientes);
router.get('/pre-registro', controller.getPreRegistros);
router.post('/pruebas', controller.registerPruebas);
router.get('/pruebas', controller.getPruebas);
router.get('/:id', controller.getPreRegistroById);
router.put('/:id', controller.updateState);

module.exports = router;