const mongoose = require('mongoose');

const connection = async() => {
    mongoose.set('strictQuery', false);
    mongoose.connect("mongodb+srv://admin:admin@cluster0.ubbkqwz.mongodb.net/referencia?retryWrites=true&w=majority")
    .then((result) => 
        console.log('Database connected')
    )
    .catch(err => 
        console.log(err)
    )
}

module.exports = connection;