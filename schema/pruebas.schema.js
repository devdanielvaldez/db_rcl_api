const { Schema, model, ObjectId } = require('mongoose');

const PruebasSchema = new Schema({
    nombre: String,
    costo: String
},
{
    timestamps: true,
    versionKey: false,
    collection: "pruebas"
});

const Pruebas = model('pruebas', PruebasSchema);

module.exports = Pruebas;
