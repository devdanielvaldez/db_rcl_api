const { Schema, model, ObjectId } = require('mongoose');
const mongoose = require('mongoose');

const PacientesSchema = new Schema({
    name: String,
    lastName: String,
    email: String,
    dependiente: Boolean
},
{
    timestamps: true,
    versionKey: false,
    collection: "pacientes"
});

const Pacientes = model('pacientes', PacientesSchema);

module.exports = Pacientes;
