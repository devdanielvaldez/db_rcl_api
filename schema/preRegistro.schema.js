const { Schema, model, ObjectId } = require('mongoose');
const mongoose = require('mongoose');

const PreRegistroSchema = new Schema({
    tipoFactura: String,
    pruebas: [Object],
    pruebasPrivadas: [Object],
    metodoDePago: String,
    estado: String,
    codigo: String,
    tipo: String
},
{
    timestamps: true,
    versionKey: false,
    collection: "PreRegistro"
});

const PreRegistro = model('PreRegistro', PreRegistroSchema);

module.exports = PreRegistro;
