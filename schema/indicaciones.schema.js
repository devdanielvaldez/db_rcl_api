const { Schema, model, ObjectId } = require('mongoose');
const mongoose = require('mongoose');

const IndicacionesSchema = new Schema({
    indicacion: String
},
{
    timestamps: true,
    versionKey: false,
    collection: "indicaciones"
});

const Indicaciones = model('indicaciones', IndicacionesSchema);

module.exports = Indicaciones;
